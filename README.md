# images-tests

Test repository to process images.

### Dependencies
Run `npm install`.

Install ImageMagick.

Previously attempted to also make it work with:
* https://www.npmjs.com/package/sharp
Couldn't work without a 64 bit nodejs.
* https://www.npmjs.com/package/jimp
Very low performance and high memory, crashing the function.

You can try to implement the `lib/ImageHandler.js` using these packages and see for yourself.

### Run Test

To run locally `node run`.

To run as an http triggered azure function, `{host}/test/{filename}`.
* `filename` = a file in the `input` dir.

Output files will be written into `output` dir.