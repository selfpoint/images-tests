'use strict';

/*********************
 * Node
 * */
const fs = require('fs'),
    path = require('path'),
    outputDirPath = path.join(__dirname, '..', 'output');

/*********************
 * node_modules
 * */
const Promise = require('bluebird'),
    temp = require('temp'),
    readChunk = require('read-chunk'),
    fileType = require('file-type');

/*********************
 * Internal
 * */
const CannotUploadError = require('./CannotUploadError'),
    ImageHandler = require('./ImageHandler');

/*********************
 * Statics
 * */
const _IMAGES_SETTINGS = require('./images-settings.json');

/*********************
 * Exports
 * */
module.exports = {
    run: runHandler
};

/**
 * Run the processor handler
 * @public
 *
 * @param {ImagesProcessorHandlerOptions} options
 * @param {ImageProcessorLogger} logger
 *
 * @returns {Promise<ProcessorResult>}
 */
function runHandler(options, logger) {
    const handler = new ImageProcessorHandler(options, logger);
    return handler.run();
}

/**
 * @typedef {Object} ProcessorResult
 *
 * @property {Array<string>} paths
 * @property {number} [imagesLength] - the number of images in a 360 zip file
 */

/**
 * @typedef {Object} ImageSize
 *
 * @property {number} maxHeight
 * @property {number} maxWidth
 * @property {boolean} [watermark]
 */

/**
 * @typedef {Object} ImageSuffixParams
 *
 * @property {number} [quality]
 */

/**
 * @typedef {Object} ImageSettings
 *
 * @property {Array<string>} suffixes
 * @property {ImageSize} [sizes.large]
 * @property {ImageSize} [sizes.medium]
 * @property {ImageSize} [sizes.small]
 */

/**
 * @typedef {Object} ImagesProcessorHandlerOptions
 *
 * @property {function} [fileReadStream] -
 *  a function which returns a read stream of the file
 *  use the file read stream or the file path
 * @property {string} uploadFileName - the file name of the uploaded file (in the bucket)
 */

/**
 * @typedef {Object} ImageProcessorLogger
 *
 * @property {function} info
 * @property {function} warn
 * @property {function} error
 */

class ImageProcessorHandler {
    /**
     * @param {ImagesProcessorHandlerOptions} options
     * @param {ImageProcessorLogger} logger
     */
    constructor(options, logger) {
        this.logger = logger;

        this.imageSettings = _IMAGES_SETTINGS.image;

        this.options = options;
        this.tempPath = temp.path();
    }

    /**
     * Run.
     * @public
     *
     * @returns {Promise<ProcessorResult>}
     */
    async run() {
        try {
            await this._downloadImage();
            const suffix = await this._getSuffix();

            let result = await this._handleImage(suffix);

            await this._tryDeleteFile(this.tempPath);

            return result;
        } catch(err) {
            if (!this.options.filePath) {
                await this._tryDeleteFile(this.tempPath);
            }

            throw err;
        }
    }

    /**
     * Handle a single image file
     * @private
     *
     * @param {string} suffix
     *
     * @return {Promise<ProcessorResult>}
     */
    async _handleImage(suffix) {
        this.logger.info('Handling single image, suffix = ' + suffix);
        await this._renameExtension(suffix);
        // await this._colorSpaceToRgb(suffix);
        await this._shrinkImage(suffix);
        await this._backupFile(suffix);

        await this._uploadProductFiles(suffix);

        this.logger.info('Done handling single image');
        return {
            paths: this._getUploadedPaths()
        };
    }

    /**
     * Returns an array of the paths which are uploaded in this image handler
     * @private
     *
     * @return {Array<string>}
     */
    _getUploadedPaths() {
        const res = [];

        for (const sizeName of Object.keys(this.imageSettings.sizes)) {
            for (const suffix of this.imageSettings.suffixes) {
                res.push(this._getBucketPath(sizeName, suffix));
            }
        }

        return res;
    }

    /**
     * Try to delete a file
     * @private
     *
     * @param {string} path
     *
     * @returns {Promise<void>}
     */
    async _tryDeleteFile(path) {
        try {
            await this._deleteFile(path);
        } catch(err) {
            this.logger.error(`Can't remove file (${path}), error: ${err.stack || JSON.stringify(err)}`);
        }
    }

    /**
     * Delete a file
     * @private
     *
     * @param {string} path
     *
     * @returns {Promise<void>}
     */
    async _deleteFile(path) {
        await new Promise((resolve, reject) => {
            fs.unlink(path, err => err ? reject(err) : resolve());
        });
    }

    /**
     * Download the image from the options.fileReadStream
     * @private
     *
     * @returns {Promise<void>}
     */
    async _downloadImage() {
        try {
            this.logger.info('Downloading file');
            const readStream = this.options.fileReadStream(),
                writeStream = fs.createWriteStream(this.tempPath);

            await new Promise((resolve, reject) => {
                readStream
                    .on('error', err => reject(err))
                    .pipe(writeStream)
                    .on('error', err => reject(err))
                    .on('finish', () => resolve());
            });
            this.logger.info('Done downloading file');
        } catch (err) {
            if (err instanceof URIError) {
                throw new CannotUploadError(`got URIError when trying to download file`);
            }

            throw err;
        }
    }

    async _renameExtension(suffix) {
        const newTempPath = `${this.tempPath}.${suffix}`;
        await new Promise((resolve, reject) => {
            fs.rename(this.tempPath, newTempPath, err => err ? reject(err) : resolve());
        });
        this.tempPath = newTempPath;
    }

    /**
     * Shrink the backup/downloaded image to it's max size
     * @private
     *
     * @param {string} suffix
     *
     * @returns {Promise<void>}
     */
    async _shrinkImage(suffix) {
        this.logger.info('Shrinking image');

        const imageHandler = new ImageHandler(this.tempPath, {
            suffixParams: _IMAGES_SETTINGS.suffixes[suffix]
        });
        await imageHandler.resize(_IMAGES_SETTINGS.backup);
        await imageHandler.save();
        this.logger.info('Done shrinking image');
    }

    /**
     * Returns the suffix of the image
     * @private
     *
     * @returns {Promise<string>}
     */
    async _getSuffix() {
        try {
            this.logger.info('Getting suffix');
            const buffer = await readChunk(this.tempPath, 0, fileType.minimumBytes),
                mime = fileType(buffer);

            this.logger.info('Done getting suffix');
            return mime.ext;
        } catch(err) {
            throw new CannotUploadError(err);
        }
    }

    /**
     * Upload the file to the backup bucket in the backup path
     * @private
     *
     * @param {string} suffix
     *
     * @returns {Promise<void>}
     */
    async _backupFile(suffix) {
        this.logger.info('Uploading image to backup');

        // simulate upload to google storage backup bucket
        await _writeFile(this.tempPath, `backup/${this.options.uploadFileName}${suffix ? '.' + suffix : ''}`);
        this.logger.info('Done uploading image to backup');
    }

    /**
     * Set the colorspace as RGB
     * @private
     *
     * @param {string} suffix
     *
     * @returns {Promise<void>}
     */
    async _colorSpaceToRgb(suffix) {
        const imageHandler = new ImageHandler(this.tempPath);
        try {
            this.logger.info('Colorspace RGB');
            await imageHandler.colorspaceRGB();
            await imageHandler.save();
            this.logger.info('Done Colorspace RGB');
        } catch(err) {
            if (err.message.startsWith('vips_colourspace: no known route from')) {
                this.winston.warn(`Couldn't colorspace to rgb, Error: ${err.message}`);
            } else {
                throw err;
            }
        }
    }

    /**
     * Resize and prepare images of the sizes and upload them
     * @private
     *
     * @param {string} suffix
     *
     * @returns {Promise<void>}
     */
    async _uploadProductFiles(suffix) {
        await Promise.map(Object.entries(this.imageSettings.sizes), async ([sizeName, sizeValue]) => {
            await Promise.map(this.imageSettings.suffixes, async toSuffix => {
                const suffixParams = _IMAGES_SETTINGS.suffixes[toSuffix];

                const sizePath = await this._uploadProductSize({
                    originalSuffix: suffix,
                    sizeName,
                    sizeValue,
                    suffix: toSuffix,
                    suffixParams
                });

                // delete the temp file
                await this._deleteFile(sizePath);
            });
        });
    }

    /**
     * Resize, process and upload size image
     * @private
     *
     * @param {string} options.originalSuffix
     * @param {string} options.sizeName
     * @param {ImageSize} options.sizeValue
     * @param {string} options.suffix
     * @param {ImageSuffixParams} options.suffixParams
     *
     * @returns {Promise<string>}
     */
    async _uploadProductSize(options) {
        const sizePath = temp.path({suffix: '.' + options.suffix});

        this.logger.info('Resizing size ' + JSON.stringify(options));

        const imageHandler = new ImageHandler(this.tempPath, {
            toPath: sizePath,
            suffixParams: options.suffixParams
        });

        await imageHandler.resize(options.sizeValue);

        if (options.suffix === 'jpg' && options.originalSuffix === 'png') {
            await imageHandler.whiteCanvas();
        }

        if (options.sizeValue.watermark) {
            await imageHandler.watermark();
        }

        await imageHandler.save();

        this.logger.info('Done resizing size ' + JSON.stringify(options));

        this.logger.info('Uploading size ' + JSON.stringify(options));
        // simulate upload the file to the google storage
        await _writeFile(sizePath, this._getBucketPath(options.sizeName, options.suffix));
        this.logger.info('Done uploading size ' + JSON.stringify(options));

        return sizePath;
    }

    /**
     * Return the bucket path of the given size and suffix
     * @private
     *
     * @param {string} sizeName
     * @param {string} suffix
     *
     * @return {string}
     */
    _getBucketPath(sizeName, suffix) {
        return `public/${sizeName}/${this.options.uploadFileName}${suffix ? '.' + suffix : ''}`;
    }
}

async function _writeFile(srcFile, destFile) {
    const outputFullPath = path.join(outputDirPath, destFile);

    await _makeFileDir(outputFullPath);

    const readStream = fs.createReadStream(srcFile),
        writeStream = fs.createWriteStream(outputFullPath);

    await new Promise((resolve, reject) => {
        readStream.pipe(writeStream)
            .on('error', err => reject(err))
            .on('finish', () => resolve());
    });
}

async function _makeFileDir(destFile) {
    const outputDir = path.dirname(destFile);

    let stats;
    try {
        stats = await new Promise((resolve, reject) => {
            fs.lstat(outputDir, (err, stats) => err ? reject(err) : resolve(stats));
        });
    } catch (err) {
        if (err.code !== 'ENOENT') {
            throw err;
        }
    }

    if (!stats || !stats.isDirectory()) {
        await new Promise((resolve, reject) => {
            fs.mkdir(outputDir, {recursive: true}, (err) => err ? reject(err) : resolve());
        });
    }
}