'use strict';

/*********************
 * Node
 * */

const path = require('path');

/*********************
 * node_modules
 * */

const Promise = require('bluebird'),
    sharp = require('sharp');

sharp.cache(false);

/*********************
 * Internal
 * */

const _WATERMARK_PATH = path.join(__dirname, '../resources/watermark.png');

class ImageHandler {
    /**
     * @param {string|Buffer} pathOrBuffer
     * @param {object} [saveParams={}]
     * @param {string} [saveParams.toPath=path]
     * @param {ImageSuffixParams} [saveParams.suffixParams={}]
     */
    constructor(pathOrBuffer, saveParams = {}) {
        this.saveParams = saveParams;
        this.image = sharp(pathOrBuffer);

        if (typeof(pathOrBuffer) === 'string') {
            this.path = pathOrBuffer;
        }
    }

    async resize(size) {
        const dimensions = await this.getDimensions();

        if (size.maxWidth && dimensions.width > size.maxWidth) {
            const wDifferential = dimensions.width / size.maxWidth;
            dimensions.width /= wDifferential;
            dimensions.height /= wDifferential;
        }

        if (size.maxHeight && dimensions.height > size.maxHeight) {
            const hDifferential = dimensions.height / size.maxHeight;
            dimensions.width /= hDifferential;
            dimensions.height /= hDifferential;
        }

        dimensions.width = Math.round(dimensions.width);
        dimensions.height = Math.round(dimensions.height);

        await this.image.resize({
            width: dimensions.width,
            height: dimensions.height,
            kernel: sharp.kernel.cubic
        });
    }

    async getDimensions() {
        if (this._dimensions) {
            return this._dimensions;
        }

        const metadata = await this.image.metadata();

        return this._dimensions = {
            width: metadata.width,
            height: metadata.height
        };
    }

    async whiteCanvas() {
        const dimensions = await this.getDimensions(),
            channels = 4,
            rgbaPixel = 0xfff;

        const canvas = sharp(Buffer.alloc(dimensions.width * dimensions.height * channels, rgbaPixel), {
            raw: {
                width: dimensions.width,
                height: dimensions.height,
                channels
            }
        });

        await canvas.toFormat('jpeg').composite([{
            input: await this.toBuffer(),
            left: 0,
            top: 0
        }]);
        this.image = canvas;
    }

    async toBuffer() {
        return await this.image.toBuffer();
    }

    async watermark() {
        const dimensions = await this.getDimensions();

        const watermark = new ImageHandler(_WATERMARK_PATH);
        await watermark.resize({
            maxWidth: dimensions.width * 0.5,
            maxHeight: dimensions.height * 0.15
        });

        const watermarkDimensions = await watermark.getDimensions();
        await this.image.composite([{
            input: await watermark.toBuffer(),
            left: Math.round((dimensions.width - watermarkDimensions.width) / 2),
            top: Math.round((dimensions.height - watermarkDimensions.height) / 2)
        }]);
    }

    async save() {
        const fileHandler = new ImageHandler(await this.toBuffer()),
            toPath = this.saveParams.toPath || this.path;
        await fileHandler.toFormat(path.extname(toPath).replace('.', ''), this.saveParams.suffixParams);
        await fileHandler.image.toFile(toPath);
        return toPath;
    }

    async toFormat(suffix, options) {
        if (suffix === 'jpg') {
            suffix = 'jpeg';
        }

        this._format = suffix;
        await this.image.toFormat(suffix, options);
    }

    async colorspaceRGB() {
        await this.image.toColorspace('rgb');
    }
}

module.exports = ImageHandler;
