'use strict';

module.exports = {
    get imageProcessorHandler() {
        return require('./image-processor-handler');
    },
    get CannotUploadError() {
        return require('./CannotUploadError');
    }
};