'use strict';

class CannotUploadError extends Error {
    constructor(err) {
        if (err instanceof Error) {
            super(err.message);
            this.originalError = err;
        } else {
            super(err);
        }

        this.name = 'CannotUploadError';
    }
}

module.exports = CannotUploadError;