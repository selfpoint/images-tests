'use strict';

const Promise = require('bluebird'),
    fs = require('fs'),
    path = require('path'),
    inputDirPath = path.join(__dirname, '..', 'input'),
    functionHandler = require('../function');

(async () => {
    const inputFiles = await _readInput();

    console.log('inputFiles =', inputFiles);
    await Promise.map(inputFiles, async fileName => {
        await functionHandler({
            log: console.log,
            bindingData: {
                name: fileName
            }
        });
    }, {
        concurrency: 2
    });
})();

function _readInput() {
    return new Promise((resolve, reject) => {
        fs.readdir(inputDirPath, (err, names) => err ? reject(err) : resolve(names));
    });
}

process
// Catch all errors
    .on('uncaughtException', (err, origin) => {
        console.error(`Uncaught Exception: ${err.message},\nstack: ${err.stack}`, { origin });
    })
    // Catch all Native Promise Unhandled Rejection.
    .on('unhandledRejection', (reason, promise) => {
        console.error(`Unhandled Rejection: ${reason},\nstack: ${reason.stack}`, { promise });
    })
    // Log every exist
    .on('exit', (code) => {
        console.error(`Process finished with exit code: ${code}`);
    });