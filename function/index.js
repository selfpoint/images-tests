'use strict';

const lib = require('../lib'),
    fs = require('fs'),
    path = require('path'),
    inputDirPath = path.join(__dirname, '..', 'input');

module.exports = async function (context) {
    context.log(`Http Trigger - Testing image ${context.bindingData.name}`);
    context.log(`arch: ${process.arch}, version: ${process.version}, execPath: ${process.execPath}, cwd: ${process.cwd()}`);
    await lib.imageProcessorHandler.run({
        fileReadStream: () => fs.createReadStream(path.join(inputDirPath, context.bindingData.name)),
        uploadFileName: context.bindingData.name.split('.')[0]
    }, {
        info: context.log,
        error: (...args) => context.log('Error', ...args)
    });
    context.log(`Http Trigger - Finished testing image ${context.bindingData.name}`);
};